﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Security.Cryptography;

namespace JLib.Extension
{
    public static class StringExtension
    {
        /// <summary>
        /// 扩展方法:将字符串按长度切割
        /// </summary>
        /// <param name="oldStr"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        public static string Cut(this string oldStr, int len = 10)
        {
            var oldStrLen = oldStr.Length;
            if (oldStrLen <= len)
            {
                return oldStr;
            }
            else
            {
                return oldStr.Substring(0, len - 3) + "...";
            }
        }

        /// <summary>
        /// 扩展方法：将字符串逆转
        /// </summary>
        /// <param name="oldStr"></param>
        /// <returns></returns>
        public static string Reverse(this string oldStr)
        {
            char[] temp = oldStr.ToCharArray();
            Array.Reverse(temp);
            return new string(temp);
        }

        /// <summary>
        /// 扩展方法:手机号转化成189****6547形式
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns>'Error': 号码格式出错</returns>
        public static string GetHidePhoneNumber(string phoneNumber)
        {
            if (phoneNumber.Length == 11)
            {
                return phoneNumber.Substring(0, 3) + "****" + phoneNumber.Substring(7, 4);
            }
            else
            {
                return "Error";
            }
        }



        #region 字符串验证

        /// <summary>
        /// 扩展方法:判断字符串是否只由数字组成
        /// </summary>
        /// <param name="oldStr"></param>
        /// <returns></returns>
        public static bool IsNumber(this string oldStr)
        {
            return Regex.IsMatch(oldStr, @"^[0-9]+$");
        }

        /// <summary>
        /// 扩展方法:判断字符串是否只由数字或字母组成
        /// </summary>
        /// <param name="oldStr"></param>
        /// <returns></returns>
        public static bool IsNumberOrLetter(this string oldStr)
        {
            return Regex.IsMatch(oldStr, @"^[A-Za-z0-9]+$");
        }

        /// <summary>
        /// 扩展方法:判断字符串是否只由数字或字母或汉字组成
        /// </summary>
        /// <param name="oldStr"></param>
        /// <returns></returns>
        public static bool IsNumberOrLetterOrChinese(this string oldStr)
        {
            return Regex.IsMatch(oldStr, @"[a-zA-Z0-9\u4e00-\u9fa5]{1,50}");
        }

        /// <summary>
        /// 扩展方法:判断字符串是否只由汉字组成
        /// </summary>
        /// <param name="oldStr"></param>
        /// <returns></returns>
        public static bool IsChinese(this string oldStr)
        {
            return Regex.IsMatch(oldStr, @"^[\u4e00-\u9fa5]+$");
        }

        /// <summary>
        /// 扩展方法:判断字符串是否是Email
        /// </summary>
        /// <param name="oldStr"></param>
        /// <returns></returns>
        public static bool IsEmail(this string oldStr)
        {
            return Regex.IsMatch(oldStr, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }

        /// <summary>
        /// 扩展方法:判断字符串是否是身份证号
        /// </summary>
        /// <param name="oldStr"></param>
        /// <returns></returns>
        public static bool IsIDCard(this string oldStr)
        {
            return Regex.IsMatch(oldStr, @"^(^\d{18}$)|(^\d{15}$)");
        }

        /// <summary>
        /// 扩展方法:判断字符串是否是手机号
        /// </summary>
        /// <param name="oldStr"></param>
        /// <returns></returns>
        public static bool IsMobile(this string oldStr)
        {
            return Regex.IsMatch(oldStr, @"(^189\d{8}$)|(^13\d{9}$)|(^15\d{9}$)");
        }
        #endregion
        
        #region 字符串加密
        /// <summary>
       /// 扩展方法:字符串加密
       /// </summary>
       /// <param name="oldStr"></param>
       /// <returns>加密后的字符串</returns>
        public static string CreatePassword(this string oldStr)
        {
            /*NOTE:
             * 一个密码字符串由数字和母组成
             * STEP1: 产生一个key,范围在11-99之间
             * STEP2: 移位值=(字符串长度+key)%(key个十位相加值)
             * STEP3: 数字=数字左移移位值位，字母=字母右移位值位
             * STEP4: key转成十六进制(2位)加在结果后面
            */
            int key,displacement;
            repeat: key = new Random().Next(11,99);
            displacement = (oldStr.Length + key) % (key % 10 + key / 10);
            if (displacement == 0) goto repeat;
            string result = "";

            char[] items = oldStr.ToCharArray();
            foreach(var i in items) 
            {
                if (i-48 >= 0 && i-48 <= 9)
                {
                    if((i-displacement)>='0')
                    {
                        result += (char)(i-displacement);
                    }
                    else
                    {
                        result += (char)(58 - (48 - (i - displacement)));
                    }
                }
                else if(i >= 'a' && i <='z')
                {
                    result += (char)((i - 97 + displacement) % 26 + 97);
                }
                else if (i >= 'A' && i <= 'Z')
                {
                    result += (char)((i - 65 + displacement) % 26 + 65);
                }
            }
            return result + key.ToString("X2");
        }

        /// <summary>
        /// 扩展方法:字符串解密
        /// </summary>
        /// <param name="oldStr"></param>
        /// <returns>解密后的字符串</returns>
        public static string RemovePassword(this string oldStr)
        {
            string handleStr = oldStr.Substring(0, oldStr.Length - 2);   
            int key = Int32.Parse(oldStr.Substring(oldStr.Length - 2), System.Globalization.NumberStyles.HexNumber);
            int dpt = (handleStr.Length + key) % (key % 10 + key / 10);
            char[] items = handleStr.ToCharArray();
            string result = "";
            foreach(var i in items)
            {
                if (i-48 >= 0 && i-48 <= 9)
                {
                    if (i + dpt <= '9')
                        result += (char)(i + dpt);
                    else
                        result += (char)(48 + ((i + dpt) - 58));
                }
                else if(i >= 'a' && i <='z')
                {
                    if (i - dpt >= 'a')
                    {
                        result += (char)(i - dpt);
                    }
                    else
                    {
                        result += (char)(123 - Math.Abs(97 - (i - dpt)));
                    }
                }
                else if (i >= 'A' && i <= 'Z')
                {
                    if (i - dpt >= 'A')
                    {
                        result += (char)(i - dpt);
                    }
                    else
                    {
                        result += (char)(81 - Math.Abs(65 - (i - dpt)));
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 扩展方法:数字加密算法
        /// Step1: oldStr(oldStr小于等于9位) * key各位之和(key小于等于99)
        /// Step2: 再将其与(key*key)异或
        /// Step3: 再将结果倒序输出
        /// 例如: oldStr="12345678" ,key=56
        /// Step1: 12345678 * 11 = 135802458
        /// Step2: 135802458^(56*56)=135799322
        /// 结果: 223997531
        /// </summary>
        /// <param name="oldStr">最大9位数</param>
        /// <param name="key">两位数密钥</param>
        /// <returns></returns>
        public static string ToPwd(this string oldStr,int key)
        {
            if (!oldStr.IsNumber()) {
                return "Inviad String";
            }

            if (oldStr.Length > 10)
            {
                return "Too Long";
            }

            int keyer = key / 10 + key % 10;
            string step1 = (Convert.ToInt64(oldStr)*keyer).ToString();
            string step2 = (Convert.ToInt64(step1) ^ (key * key)).ToString();
            string result = step2.Reverse();
            return result;
        }

        /// <summary>
        /// 扩展方法:数字解密密算法
        /// </summary>
        /// <param name="oldStr"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string DePwd(this string oldStr, int key)
        {
            int keyer = key / 10 + key % 10;
            string step1 = oldStr.Reverse();
            string step2 = (Convert.ToInt64(step1) ^ (key * key)).ToString();
            string result = (Convert.ToInt64(step2) / keyer).ToString();
            return result;
        }

        /// <summary>
        /// 扩展方法:将字符以MD5方式加密
        /// </summary>
        /// <param name="oldStr">加密字符串</param>
        /// <returns>加密结果</returns>
        
        #endregion



        /*----------------------------------------------方法分隔线----------------------------------------------*/
    }
}





